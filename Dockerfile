FROM node:alpine

WORKDIR /usr/src/app

COPY ["package.json", "yarn.lock", "./"]

RUN apk add yarn

RUN yarn