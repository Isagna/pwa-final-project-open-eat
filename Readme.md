Réalisation d'une PWA sur le thème d'une application livraison de repas.

Cette application devra reprend l’ensemble des concept vue en cours afin de fournir la meilleure Expérience Utilisateur possible

---

# Open Eat

## Fonctionnalité :

- Firebase (Cloud function / realtime database / Cloud Firestore)
- UI qui montre l'état de la connexion (online / Offline)
- Authentification
- Passer une commande
- Rechercher un établissement
- Favoris
- Notation (note sur 5)
- Commentaires
- Avatar utilisateur
- Image (lazy loading)
- View lazy loading
- Synchronisation (Online / offline)
- Web push notification

## Notation

- Projet
  - PWA lighthouse score (ALL)
  - Fonctionnalité bonus
    - Direct message
  - Code
  - Good UX
- Questions